CC ?= gcc
CPP ?= cpp
AR ?= ar
OBLIVCC = $(OBLIVC_PATH)/bin/oblivcc
OBLIVCH = $(OBLIVC_PATH)/src/ext/oblivc
OBLIVCA = $(OBLIVC_PATH)/_build/libobliv.a
CFLAGS+= -D _Float128=double -O3 -march=native -I/usr/include -I . -I $(SRC_PATH) -std=c99 -fopenmp #--save-temps  
LDFLAGS += -lm -lgomp -lgcrypt

SRC_PATH=src/
LIB_OUT_PATH=build/lib/
NMLIB = $(LIB_OUT_PATH)/libnoisymax.a
DEPS=ackutil.o
OBJS=$(DEPS) push_only_ostack.oo predfunctions.oo pop_only_rostack.oo odofunctions.oo utils.oo onmfunctions.oo

TEST_PATH=tests/
TEST_OUT_PATH=build/tests/
TEST_DEPS=test_main.o
TEST_BINS = test_push_ostk test_predfunctions test_pop_rostk bench_push_ostk bench_pop_rostk bench_coinflip\
		bench_pred test_coinflip test_odo_sample test_onm_sample bench_odo_sample_0_0866 bench_onm_sample_0_0866\
		bench_onm_sample_0_1 noisy_max_0_0866

default: $(NMLIB) tests

tests: $(TEST_BINS:%=$(TEST_OUT_PATH)/%)

$(TEST_BINS:%=$(TEST_OUT_PATH)/%): $(TEST_OUT_PATH)/%: $(TEST_PATH)/%.oo $(TEST_DEPS:%=$(TEST_PATH)/%) $(NMLIB)
	mkdir -p $(TEST_OUT_PATH)
	$(OBLIVCC) -o $@ $(OBLIVCA) $^ $(LDFLAGS)

$(NMLIB): $(OBJS:%=$(SRC_PATH)/%) # put all of OBJS into archive file
	mkdir -p $(LIB_OUT_PATH)
	$(AR) rcs $@ $^

-include $($(patsubst %.oo,%.od,$(OBJS:.o=.d)):%=$(SRC_PATH)/%) $(TEST_BINS:%=$(TEST_PATH)/%.od)

%.o: %.c
	$(CC) -c $(CFLAGS) $*.c -o $*.o -I $(OBLIVCH)
	$(CPP) -MM $(CFLAGS) $*.c -I $(OBLIVCH) > $*.d

%.oo: %.oc
	$(OBLIVCC) -c $(CFLAGS) $*.oc -o $*.oo
	$(CPP) -MM $(CFLAGS) $*.oc -MT $*.oo > $*.od

clean:
	rm -f $(OBJS:%=$(SRC_PATH)/%) $(patsubst %.oo,$(SRC_PATH)/%.od,$(patsubst %.o,$(SRC_PATH)/%.d,$(OBJS))) $(NMLIB)
	rm -f $(TEST_BINS:%=$(TEST_OUT_PATH)/%) $(TEST_DEPS:%=$(TEST_PATH)/%) $($(pasubst %.oo, %.od, $(TEST_DEPS)):%=$(TEST_PATH)/%) $(TEST_BINS:%=$(TEST_PATH)/%.oo) $(TEST_BINS:%=$(TEST_PATH)/%.od)
