Securely Sampling Biased Coins with Applications to Differential Privacy
=====

This repository provides code to accompany the paper Securely Sampling Biased Coins with Applications to Differential Privacy by Jeffrey Champion, abhi shelat, and Jonathan Ullman which was accepted to the 26th ACM Conference on Computer and Communications Security (CCS). Our code takes the form of a library, along with a small number of testing and benchmarking applications. The library is written in [obliv-c](https://github.com/samee/obliv-c/), a c-derived language for secure multiparty computation.


Features
=====
This distribution is intended to provide reference implementations of the MNM (labeled onm in the code) and ODO sampling methods (with utils to compute noisy max), which are described and compared in the aforementioned paper. Naturally, we also have full implementations for the push-only and pop-only stacks described in the paper (inspired by _[Circuit Structures for Improving Efficiency of Security and Privacy Tools](http://www.ieee-security.org/TC/SP2013/papers/4977a493.pdf)_), as well as circuits for the predicate functions used for the values of epsilon evaluated in the paper.
 

Installing
=====

1. You must first build [obliv-c](https://github.com/samee/obliv-c/), though it need not be installed in any particular location.

2. To compile, set the path to obliv-c's main project directory via `export OBLIVC_PATH=<path to obliv-c>`, then run `make`.


Project Organization
=====

Source for this project is divided into two directories: `src` contains code for the primary library, while `tests` contains code for tests and benchmarks. The library will be compiled to `build/lib/libnoisymax.a`, and all testing and benchmarking binaries are found in `build/tests`.


Reproducing Results
=====

For the purpose of reproducing the results we report in our paper, we provide a suite of benchmark scripts in the `tools/bench` directory. Each script must be executed on one machine as a server, and on another as a client. Scripts will run as server by default, and will output data and summaries to the `benchmark_results` directory. Adding the `-c <address>` flag will cause the script to run as a client and connect to the specified server.


Running Tests and Benchmarks Manually
=====

Each of our benchmarking and testing programs (that is, the binaries, not the benchmarking scripts described above) have individual options for adjusting the parameters relevant to that particular test or benchmark. These can be found by running the programs with the `-h` flag. In addition, there are a few standard parameters shared by all of the included programs, as well as the benchmarking scripts:

* `-h` prints a list of program-specific flags (our main flags for sampling are described in detail in the paper).
* `-p <number>` determines the port on which the program will listen (for servers) or connect (for clients). The default port is 54321.
* `-c <address>` instructs the program to run as a client, and connect to the server at `<address>`. By default, the program will run as a server.
* `-i <number>` (benchmarks only) instructs a benchmark to run for `<number>` iterations, and record results for all of them.

Due to the difficulty of calculating our sampling parameters, we provide a [sage](http://www.sagemath.org/download.html) script in the `tools/findparams` directory for this purpose. Given inputs epsilon, delta, n (as in differential privacy), this script outputs the flags needed for both MNM and ODO benchmark programs, as well as some complexity analysis. We note that when using the predicate method (make-batch 2 in the paper) for epsilon other than what we provide, one must construct predicate circuits that output the binary expansion of the probability that the ith bit of a Geo(2/epsilon) sample is 1 for i=0,...,k. The NIST circuit generator for 6 variable predicates based on truth tables can be found [here](https://github.com/usnistgov/Circuits).    


Building on this Work
=====

We encourage others to improve our work and to integrate it with their own applications. As such, we provide it under the 3-clause BSD license. For ease of integration, our code takes the form of a library, to which other software can link directly.


