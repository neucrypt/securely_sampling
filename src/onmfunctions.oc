#include "onmfunctions.oh"
#include "push_only_ostack.oh"
#include "pop_only_rostack.oh"
#include "utils.oh"
#include "ackutil.h"
#include <copy.oh>
#include <obliv.oh>

obliv uint16_t* sample_onm(obliv bool* rands, uint8_t k, uint16_t cg, uint16_t g, uint16_t rem_cg, uint16_t rem_g, uint32_t n, 
			obliv bool* biases, uint16_t len, bool testing, uint8_t numReal, frozen bool isPred, obliv bool (*pred)(uint8_t, obliv uint16_t, int)) {

	push_ostk* cstk = push_ostk_new_static(&ocCopyBool, g);
	push_ostk* rem_cstk = push_ostk_new_static(&ocCopyBool, rem_g);
	pop_rostk* biases2[numReal];
	int high = len - 1;
	int bits = 1; 
	while(high >>= 1) {bits++;} //find number of bits needed for count
	obliv uint16_t count = 0;
	if (!isPred) for (int i = 0; i < numReal; i++) biases2[i] = pop_rostk_from_array(&ocCopyBool, len, biases + i*len);
	obliv uint16_t* noise = calloc(n, sizeof(obliv uint16_t));
	obliv bool* group = malloc(g*sizeof(obliv bool));
	int feed = 8*k*cg; // represents the number of rands to be fed at a time at most
	int track = 0; //tracks rand bit when not testing
	obliv bool* rnds = malloc(feed*sizeof(obliv bool));
	uint64_t r = 0; // keeps track of which random bit to use when testing
	obliv bool xor, rnd, bias;
	bool b;
	uint32_t numgroups = n/g + ((n%g) != 0);
	obliv bool* temp1 = malloc(feed*sizeof(obliv bool));
	obliv bool* temp2 = malloc(feed*sizeof(obliv bool));
	bool* raw = malloc(feed*sizeof(bool));
	for (uint32_t i = 0; i < numgroups-1; i++) {
		if (!testing && (i%8 == 0)) {
			if (numgroups - i < 8) feed = k*cg*(numgroups - i - 1) + k*rem_cg; //less than 10 groups to make including rem group
			// feed random bits and xor
			for (int z = 0; z < feed; z++) raw[z] = rand()&1;
			feedOblivBoolArray(temp1, raw, feed, 0);
			feedOblivBoolArray(temp2, raw, feed, 1);
			for (int z = 0; z < feed; z++) rnds[z] = temp1[z] ^ temp2[z]; //key step is to reassign rnds to xor of 2 parties
			track = 0;
		}
		for (uint8_t j = 0; j < k; j++) {
			for (uint16_t w = 0; w < cg; w++) {
				if (!testing) rnd = *(rnds + track);
				else rnd = *(rands + r);
				// comp bias and rand, obliv push/reset
				if (j < numReal) {
					if (isPred) bias = pred(j, count, len);
					else pop_rostk_pop(&bias, biases2[j]);
					xor = bias^rnd;
					if (isPred) incr_n(&count, bits);
					obliv if (xor) {
						push_ostk_push(&bias, cstk);
						if (isPred) count = 0;
						else pop_rostk_reset(biases2[j]);
					}
				}
				// periodic bias case
				else {
					bias = (count >> (j-numReal))&1;
					xor = bias^rnd;
					incr_n(&count, bits);
					obliv if (xor) {
						push_ostk_push(&bias, cstk);
						count = 0;
					}
				}
				r++;
				track++;
			}
			// purge and concat noise
			push_ostk_purge(group, cstk);
			if (!isPred && j<numReal) pop_rostk_reset(biases2[j]);
			count = 0;	
			for (uint16_t v = 0; v < g; v++) {
				*(noise + i*g + v) ^= (((obliv uint16_t)(*(group + v))) << j);
			}
		}
	}
			free(temp1);
			free(temp2);
			free(raw);
	
	// remainder group, same as above but with potentially smaller stack size
	obliv bool* group_rem = malloc(rem_g*sizeof(obliv bool));
	for (uint8_t j = 0; j < k; j++) {
		for (uint16_t w = 0; w < rem_cg; w++) {
			if (!testing) rnd = *(rnds + track);
			else rnd = *(rands + r);
			if (j < numReal) {
				if (isPred) bias = pred(j, count, len);
				else pop_rostk_pop(&bias, biases2[j]);
				xor = bias^rnd;
				if (isPred) incr_n(&count, bits);
				obliv if (xor) {
					push_ostk_push(&bias, rem_cstk);
					if (isPred) count = 0;
					else pop_rostk_reset(biases2[j]);
				}
			}
			else {
				bias = (count >> (j-numReal))&1;
				xor = bias^rnd;
				incr_n(&count, bits);
				obliv if (xor) {
					push_ostk_push(&bias, rem_cstk);
					count = 0;
				}
			}
			r++;
			track++;
		}
		push_ostk_purge(group_rem, rem_cstk);
		for (uint16_t v = 0; v < n % g; v++) {
			*(noise + (n/g)*g + v) ^= (((obliv uint16_t)(*(group_rem + v))) << j);
		}
	}
	return noise;
}

