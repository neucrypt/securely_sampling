#include <obliv.oh>
#include "../src/pop_only_rostack.oh"
#include "../src/utils.oh"
#include <copy.oh>
#include "test_generic.h"

// Benchmark for pop and reset ops of pop-only oblivious stack

static const char TESTNAME[] = "pop_rostk_popreset_benchmark";

#define TEXT_HELP_SUPPLEMENTARY "\
  -e \x1b[4mNUMBER\x1b[0m, --element-count=\x1b[4mNUMBER\x1b[0m \n\t\tuse stacks of \x1b[4mNUMBER\x1b[0m elements\n\n\
  -s \x1b[4mNUMBER\x1b[0m, --element-size=\x1b[4mNUMBER\x1b[0m \n\t\tuse stacks with elements containing \x1b[4mNUMBER\x1b[0m obliv bools\n\n\
  -i \x1b[4mNUMBER\x1b[0m, --samples=\x1b[4mNUMBER\x1b[0m \n\t\trun \x1b[4mNUMBER\x1b[0m iterations of the benchmark\n\n"

static const char options_string[] = "e:s:i:";
static struct option long_options[] = {
	{"element-count", required_argument, NULL, 'e'},
	{"element-size", required_argument, NULL, 's'},
	{"samples", required_argument, NULL, 'i'},
	{0, 0, 0, 0}
};

char* get_test_name() {
	return TESTNAME;
}

char* get_supplementary_options_string() {
	return options_string;
}

struct option* get_long_options() {
	return long_options;
}

void print_supplementary_help() {
	fprintf(stderr, TEXT_HELP_SUPPLEMENTARY);
}

void test_main(void*varg) {


	size_t elct = 3;
	size_t elsz = 1;
	int samples = 1;

	args_t * args_pass = varg;
	int arg;
	optind = 0; // this allows us to getopt a second time
	while ((arg = getopt_long(args_pass->argc, args_pass->argv, options_string, long_options, NULL)) != -1) {
		if (arg == 'e') {
			elct = atoll(optarg);
			if (elct <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		} else if (arg == 's') {
			elsz = atoll(optarg);
			if (elsz <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		} else if (arg == 'i') {
			samples = atoi(optarg);
			if (samples <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		} else if (arg == '?' || arg == ':') {
			if (optopt == 'e' || optopt == 's' || optopt == 'i') {
				fprintf (stderr, "Option -%c requires an argument.\n", optopt);
				return;
			} else {
				fprintf (stderr, "Option -%c not recognized.\n", optopt);
				return;
			}
		} else {
			abort();
		}
	}

	fprintf(stdout, "# POP_ROSTK POPRESET (element count, element size, sample 1 microseconds, sample 1 gates, sample 1 bytes, ...)\n");

	OcCopy cpy = ocCopyBoolN(elsz);

	uint64_t tally1 = 0;
	uint64_t tallygates1 = 0;
	uint64_t tallybytes1 = 0;
	uint64_t tally2 = 0;
	uint64_t tallygates2 = 0;
	uint64_t tallybytes2 = 0;
	obliv bool * src = calloc(elct*elsz, sizeof(obliv bool));
	for (int i = 0; i < elct*elsz; i++) {
		obliv bool b = feedOblivBool(rand()&1, 1);
		src[i] = b;
	}
	obliv bool * dest = calloc(elsz, sizeof(obliv bool));

	pop_rostk * testStk = pop_rostk_from_array(&cpy, elct, src);
	obliv uint16_t count = 0;
	int q = 1000000;
	obliv bool* test1;
	obliv bool* test2;
	bool testSeq[q];

	for (int ii = 0; ii <samples; ii ++) {	
		obliv bool real1 = feedOblivBool(rand()&1, 1);
		obliv bool real2 = feedOblivBool(rand()&1, 2);
		int64_t runtime1 = -current_timestamp();
		int64_t rungates1 = -yaoGateCount();
		int64_t runbytes1 = -tcp2PBytesSent(ocCurrentProto());

		pop_rostk_pop(dest, testStk);

		runtime1 += current_timestamp();
		rungates1 += yaoGateCount();
		runbytes1 += tcp2PBytesSent(ocCurrentProto());
		int64_t runtime2 = -current_timestamp();
		int64_t rungates2 = -yaoGateCount();
		int64_t runbytes2 = -tcp2PBytesSent(ocCurrentProto());

		obliv if(real2^real1) pop_rostk_reset(testStk);

		runtime2 += current_timestamp();
		rungates2 += yaoGateCount();
		runbytes2 += tcp2PBytesSent(ocCurrentProto());

		fprintf(stdout, "pop %llu,%llu,%llu, rst %llu,%llu,%llu ", runtime1,rungates1, runbytes1,runtime2,rungates2, runbytes2);
		fflush(stdout);
		tally1 += runtime1;
		tallygates1 += rungates1;
		tallybytes1 += runbytes1;
		tally2 += runtime2;
		tallygates2 += rungates2;
		tallybytes2 += runbytes2;
	}

	pop_rostk_free(testStk);
	fprintf(stdout, "\n");
	fprintf(stderr, "pop_rostk pop (count:%lld, size: %lld): %llu microseconds avg, %.6f gates avg, %llu bytes avg\n", elct, elsz, tally1 / samples, tallygates1/(float)samples, tallybytes1/samples);
	fprintf(stderr, "pop_rostk reset  (count:%lld, size: %lld): %llu microseconds avg, %llu gates avg, %llu bytes avg\n", elct, elsz, tally2 / samples, tallygates2/samples, tallybytes2/samples);
}
