#include <obliv.oh>
#include "../src/onmfunctions.oh"
#include "../src/odofunctions.oh"
#include "../src/predfunctions.oh"
#include "../src/utils.oh"
#include "biases.h"
#include <copy.oh>
#include "test_generic.h"

// Benchmark for noisy max with MNM or ODO sampling methods, epsilon = 0.0866

static const char TESTNAME[] = "noisy_max_0_0866_benchmark";

#define TEXT_HELP_SUPPLEMENTARY "\
  -k \x1b[4mNUMBER\x1b[0m, --sample-size=\x1b[4mNUMBER\x1b[0m \n\t\tmake samples of \x1b[4mNUMBER\x1b[0m bits\n\n\
  -l \x1b[4mNUMBER\x1b[0m, --length=\x1b[4mNUMBER\x1b[0m \n\t\tlimit bias to \x1b[4mNUMBER\x1b[0m bits\n\n\
  -n \x1b[4mNUMBER\x1b[0m, --num-samples=\x1b[4mNUMBER\x1b[0m \n\t\ttotal number of samples is \x1b[4mNUMBER\x1b[0m\n\n\
  -u \x1b[4mNUMBER\x1b[0m, --num-pushes=\x1b[4mNUMBER\x1b[0m \n\t\tpush \x1b[4mNUMBER\x1b[0m times per group\n\n\
  -g \x1b[4mNUMBER\x1b[0m, --group-size=\x1b[4mNUMBER\x1b[0m \n\t\tmake \x1b[4mNUMBER\x1b[0m coins per group\n\n\
  -v \x1b[4mNUMBER\x1b[0m, --rem-num-pushes=\x1b[4mNUMBER\x1b[0m \n\t\tpush \x1b[4mNUMBER\x1b[0m times for rem group\n\n\
  -q \x1b[4mNUMBER\x1b[0m, --rem-group-size=\x1b[4mNUMBER\x1b[0m \n\t\tmake \x1b[4mNUMBER\x1b[0m coins for rem group\n\n\
  -r \x1b[4mNUMBER\x1b[0m, --pred=\x1b[4mNUMBER\x1b[0m \n\t\tboolean for isPred: \x1b[4mNUMBER\x1b[0m\n\n\
  -t \x1b[4mNUMBER\x1b[0m, --type=\x1b[4mNUMBER\x1b[0m \n\t\ttype of sampling (0 for odo, 1 for onm)\n\n"

static const char options_string[] = "k:l:n:u:g:v:q:r:t:";
static struct option long_options[] = {
	{"sample-size", required_argument, NULL, 'k'},
	{"length", required_argument, NULL, 'l'},
	{"num-samples", required_argument, NULL, 'n'},
	{"num-pushes", required_argument, NULL, 'u'},
	{"group-size", required_argument, NULL, 'g'},
	{"rem-num-pushes", required_argument, NULL, 'v'},
	{"rem-group-size", required_argument, NULL, 'q'},
	{"pred", required_argument, NULL, 'r'},
	{"type", required_argument, NULL, 't'},
	{0, 0, 0, 0}
};

char* get_test_name() {
	return TESTNAME;
}

char* get_supplementary_options_string() {
	return options_string;
}

struct option* get_long_options() {
	return long_options;
}

void print_supplementary_help() {
	fprintf(stderr, TEXT_HELP_SUPPLEMENTARY);
}

void test_main(void*varg) {


	int k = 10;
	int len = 129;
	int n = 10000;
	int cg = 2103;
	int g = 765;
	int rem_cg = 441;
	int rem_g = 93;
	bool isPred = true;
	int samples = 1;
	bool isONM = true;

	args_t * args_pass = varg;
	int arg;
	optind = 0; // this allows us to getopt a second time
	while ((arg = getopt_long(args_pass->argc, args_pass->argv, options_string, long_options, NULL)) != -1) {
		if (arg == 'k') {
			k = atoll(optarg);
			if (k <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		} else if (arg == 'l') {
			len = atoll(optarg);
			if (len <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		} else if (arg == 'n') {
			n = atoll(optarg);
			if (n <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		} 
		else if (arg == 't') {
			isONM = atoi(optarg);
			if (isONM < 0 || isONM > 1) {
				fprintf (stderr, "Argument for -%c must be 0 or 1.\n", arg);
				return;
			}
		}
		else if (arg == 'u') {
			cg = atoi(optarg);
			if (cg <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		}
		else if (arg == 'g') {
			g = atoi(optarg);
			if (g <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		}
		else if (arg == 'v') {
			rem_cg = atoi(optarg);
			if (rem_cg <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		}
		else if (arg == 'q') {
			rem_g = atoi(optarg);
			if (rem_g <= 0) {
				fprintf (stderr, "Argument for -%c must be positive.\n", arg);
				return;
			}
		}
		else if (arg == 'r') {
			isPred = atoi(optarg);
			if (isPred < 0 || isPred > 1) {
				fprintf (stderr, "Argument for -%c must be 0 or 1.\n", arg);
				return;
			}
		} 
		else if (arg == '?' || arg == ':') {
			if (optopt == 'k' || optopt == 'l' || optopt == 'n' || optopt == 'u' || optopt == 'g' || optopt == 'v' || optopt == 'q' || optopt == 'r' || optopt == 't') {
				fprintf (stderr, "Option -%c requires an argument.\n", optopt);
				return;
			} else {
				fprintf (stderr, "Option -%c not recognized.\n", optopt);
				return;
			}
		} else {
			abort();
		}
	}
	int* mydata = malloc(n*sizeof(int)/2); // put your data here, default each party has half the data (all 0s)
	for (int i = 0; i < n/2; i++) mydata[i] = 0;
	const char* bias[] = {b16thrt2, b8thrt2, b4thrt2, bsqrt2, onethird, onefifth, one17th, one257th, period32, period64, period128};
	obliv bool* biases = malloc(len*k*sizeof(obliv bool));
	for (int i = 0; i < k*len; i++) biases[i] = *(bias[i/len] + i%len) == '0' ? false : true;
	uint64_t rand_used;
	if (isONM) rand_used = k*(n/g)*cg + k*rem_cg;
	else rand_used = k*len*n;
	rand_used += n; // one bit for each entry to decide sign
	obliv uint16_t* samps;

	int64_t rungates = -yaoGateCount();
	int64_t runbytes = -tcp2PBytesSent(ocCurrentProto());
	
	// combining data (n/2 must be changed according to how many elements each party feeds)
	obliv int* temp1 = malloc(n*sizeof(obliv int)/2); 
	obliv int* temp2 = malloc(n*sizeof(obliv int)/2); 
	feedOblivIntArray(temp1, mydata, n/2, 0); 
	feedOblivIntArray(temp2, mydata, n/2, 1);
	obliv int* data = malloc(n*sizeof(obliv int));
	// parties must agree on a way to concat lists so they are the same, default here just appends
	for (int i = 0; i < n/2; i++) { 
		data[i] = temp1[i];
		data[i+n/2] = temp2[i]; 
	}
	// sampling
	if (isONM) {
		if (isPred) samps = sample_onm(NULL, k, cg, g, rem_cg, rem_g, n, NULL, len, false, k, isPred, pred_0866);
		else samps = sample_onm(biases, k, cg, g, rem_cg, rem_g, n, NULL, len, false, 4, isPred, NULL);
	}
	else {
		samps = sample_odo(NULL, biases, k, len, n, false);
	}
	// take noisy max
	obliv uint32_t max = noisy_max(data, samps, n);

	rungates += yaoGateCount();
	runbytes += tcp2PBytesSent(ocCurrentProto());
	// reveal max to both parties and display performance stats
	int m;
	revealOblivInt(&m, max, 0);
	printf("NOISY MAX: %d", m);
	fprintf(stdout, "\n");
	fprintf(stderr, "noisy_max (k:%lld, len: %lld, n: %lld, isONM: %d): %llu gates + rands, %llu bytes\n", k, len, n, isONM, rand_used + rungates, runbytes);
}
