#!/bin/bash

# Runs all tests

set -e
CLIENT=false
TEST_PROG_ARGS=""
TEST_PREFIX="../../build/tests/test_"
TEST_PROGS=("coinflip" "odo_sample" "onm_sample" "pop_rostk" "predfunctions" "push_ostk")

while getopts ":c:" opt; do
	case $opt in
		c)
			TEST_PROG_ARGS+=" -c $OPTARG "
			CLIENT=true
			;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			exit 1
			;;
		:)
			echo "Option -$OPTARG requires an argument." >&2
			exit 1
			;;
	esac
done

for ((II=0; II<${#TEST_PROGS[*]}; II++));
do
	COMMAND="$TEST_PREFIX${TEST_PROGS[II]} $TEST_PROG_ARGS"
	eval "$COMMAND" 
	if [ "$CLIENT" = true ] ; then
		sleep 2
	fi
done
