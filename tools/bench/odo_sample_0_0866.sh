#!/bin/bash

# Used for generating the ODO data in our d-sample graph (epsilon = 0.0866, delta = 2^-60, 2^-80)
#NOTE: for our purposes, there is no difference between epsilon 0.1 and epsilon 0.0866, since no param changes are made for ODO protocol from one to the other

CLIENT=false
BENCH_PROG="../../build/tests/bench_odo_sample_0_0866"
BENCH_PROG_ARGS="-p 50000"
N=(4096 8192 16384 32768 65536 131072 262144 524288)
L=(77 78 79 80 81 82 83 84)
ITERS=(1 1 1 1 1 1 1 1)

while getopts ":c:" opt; do
	case $opt in
		c)
			BENCH_PROG_ARGS+=" -c $OPTARG "
			CLIENT=true
			;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			exit 1
			;;
		:)
			echo "Option -$OPTARG requires an argument." >&2
			exit 1
			;;
	esac
done

if [ "$CLIENT" = true ] 
then
	OUTFILE1=odo_sample_0_0866_`date '+%Y-%m-%d~%H-%M-%S'`.txt
	echo "$OUTFILE1" > t1
else
	OUTFILE2=odo_sample_0_0866_`date '+%Y-%m-%d~%H-%M-%S'`.txt
	echo "$OUTFILE2" > t2	
fi

mkdir -p ../../benchmark_results/odo_sample_0_0866/results
mkdir -p ../../benchmark_results/odo_sample_0_0866/samples

#OUTPUT_FILE1=odo_sample_0_0866_1.txt
#OUTPUT_FILE2=odo_sample_0_0866_2.txt 

if [ "$CLIENT" = true ] 
then
	RESULT=../../benchmark_results/odo_sample_0_0866/results/$OUTFILE1
	SAMPLE=../../benchmark_results/odo_sample_0_0866/samples/$OUTFILE1
else
	RESULT=../../benchmark_results/odo_sample_0_0866/results/$OUTFILE2
        SAMPLE=../../benchmark_results/odo_sample_0_0866/samples/$OUTFILE2
fi

#SAMPLE_FILE=../../benchmark_results/push_only_ostack/samples/$OUTPUT_FILE_NAME
#RESULT_FILE=../../benchmark_results/push_only_ostack/results/$OUTPUT_FILE_NAME

touch $SAMPLE
touch $RESULT

set -e
#this part is delta=2^-60
for ((II=0; II<${#ITERS[*]}; II++));
do
	COMMAND="$BENCH_PROG $BENCH_PROG_ARGS -k 11 -l ${L[II]} -n ${N[II]} -i ${ITERS[II]}"
	echo "COMMAND: $COMMAND"
	echo "COMMAND: $COMMAND" >> "$RESULT"
	eval "$COMMAND" >> "$SAMPLE" 2>> "$RESULT"
	if [ "$CLIENT" = true ] ; then
		sleep 5
	fi
done
L2=(97 98 99 100 101 102 103 104)
#this part its delta=2^-80
for ((II=0; II<${#ITERS[*]}; II++));
do
	COMMAND="$BENCH_PROG $BENCH_PROG_ARGS -k 11 -l ${L2[II]} -n ${N[II]} -i ${ITERS[II]}"
	echo "COMMAND: $COMMAND"
	echo "COMMAND: $COMMAND" >> "$RESULT"
	eval "$COMMAND" >> "$SAMPLE" 2>> "$RESULT"
	if [ "$CLIENT" = true ] ; then
		sleep 5
	fi
done
