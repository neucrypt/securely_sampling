#!/bin/bash

# Used for generating the MNM data in our d-sample graph (epsilon = 0.0866, delta = 2^-60, 2^-80)

CLIENT=false
BENCH_PROG="../../build/tests/bench_onm_sample_0_0866"
BENCH_PROG_ARGS="-p 50000"
N=(4096 8192 16384 32768 65536 131072 262144 524288)
U=(1941 3629 1948 3638 6929 6935 6941 6947)
G=(765 1533 765 1533 3069 3069 3069 3069)
L=(78 79 80 81 82 83 84 85)
V=(1066 1941 1071 1948 3638 6935 3647 6947)
Q=(381 765 381 765 1533 3069 1533 3069)
ITERS=(3 3 3 3 3 3 3 3)
U2=(2009 3719 7038 3727 7048 7054 7059 7065)
G2=(765 1533 3069 1533 3069 3069 3069 3069)
L2=(98 99 100 101 102 103 104 105)
V2=(1118 2009 3719 2015 3727 7054 3735 7065)
Q2=(381 765 1533 765 1533 3069 1533 3069)

L3=(58 59 60 61 62 63 64 65)
U3=(1864 3526 1872 3537 3542 3547 3553 3558)
G3=(765 1533 765 1533 1533 1533 1533 1533)
V3=(1007 1864 1013 1872 3542 3547 64 65)
Q3=(381 765 381 765 1533 1533 3 3)

L4=(118 119 120 121 122 123 124 125)
U4=(2070 3800 7147 3808 7156 7161 7166 7172)
G4=(765 1533 3069 1533 3069 3069 3069 3069)
V4=(1165 2070 3800 2076 3808 7161 3815 7172)
Q4=(381 765 1533 765 1533 3069 1533 3069)

while getopts ":c:" opt; do
	case $opt in
		c)
			BENCH_PROG_ARGS+=" -c $OPTARG "
			CLIENT=true
			;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			exit 1
			;;
		:)
			echo "Option -$OPTARG requires an argument." >&2
			exit 1
			;;
	esac
done

if [ "$CLIENT" = true ] 
then
	OUTFILE1=onm_sample_0_0866_`date '+%Y-%m-%d~%H-%M-%S'`.txt
	echo "$OUTFILE1" > t3
else
	OUTFILE2=onm_sample_0_0866_`date '+%Y-%m-%d~%H-%M-%S'`.txt
	echo "$OUTFILE2" > t4	
fi

mkdir -p ../../benchmark_results/onm_sample_0_0866/results
mkdir -p ../../benchmark_results/onm_sample_0_0866/samples

#OUTPUT_FILE1=onm_sample_0_0866_1.txt
#OUTPUT_FILE2=onm_sample_0_0866_2.txt 

if [ "$CLIENT" = true ] 
then
	RESULT=../../benchmark_results/onm_sample_0_0866/results/$OUTFILE1
	SAMPLE=../../benchmark_results/onm_sample_0_0866/samples/$OUTFILE1
else
	RESULT=../../benchmark_results/onm_sample_0_0866/results/$OUTFILE2
        SAMPLE=../../benchmark_results/onm_sample_0_0866/samples/$OUTFILE2
fi

#SAMPLE_FILE=../../benchmark_results/push_only_ostack/samples/$OUTPUT_FILE_NAME
#RESULT_FILE=../../benchmark_results/push_only_ostack/results/$OUTPUT_FILE_NAME

touch $SAMPLE
touch $RESULT

set -e

#delta-2^-40
for ((II=0; II<${#ITERS[*]}; II++));
do
	COMMAND="$BENCH_PROG $BENCH_PROG_ARGS -k 10 -l ${L3[II]} -u ${U3[II]} -g ${G3[II]} -v ${V3[II]} -q ${Q3[II]} -n ${N[II]} -r 1"
	echo "COMMAND: $COMMAND"
	echo "COMMAND: $COMMAND" >> "$RESULT"
	eval "$COMMAND" >> "$SAMPLE" 2>> "$RESULT"
	if [ "$CLIENT" = true ] ; then
		sleep 5
	fi
done
#delta=2^-60
for ((II=0; II<${#ITERS[*]}; II++));
do
	COMMAND="$BENCH_PROG $BENCH_PROG_ARGS -k 11 -l ${L[II]} -u ${U[II]} -g ${G[II]} -v ${V[II]} -q ${Q[II]} -n ${N[II]} -r 1"
	echo "COMMAND: $COMMAND"
	echo "COMMAND: $COMMAND" >> "$RESULT"
	eval "$COMMAND" >> "$SAMPLE" 2>> "$RESULT"
	if [ "$CLIENT" = true ] ; then
		sleep 5
	fi
done
#delta=2^-80
for ((II=0; II<${#ITERS[*]}; II++));
do
	COMMAND="$BENCH_PROG $BENCH_PROG_ARGS -k 11 -l ${L2[II]} -u ${U2[II]} -g ${G2[II]} -v ${V2[II]} -q ${Q2[II]} -n ${N[II]} -r 1"
	echo "COMMAND: $COMMAND"
	echo "COMMAND: $COMMAND" >> "$RESULT"
	eval "$COMMAND" >> "$SAMPLE" 2>> "$RESULT"
	if [ "$CLIENT" = true ] ; then
		sleep 5
	fi
done
#delta=2^-100
for ((II=0; II<${#ITERS[*]}; II++));
do
	COMMAND="$BENCH_PROG $BENCH_PROG_ARGS -k 11 -l ${L4[II]} -u ${U4[II]} -g ${G4[II]} -v ${V4[II]} -q ${Q4[II]} -n ${N[II]} -r 1"
	echo "COMMAND: $COMMAND"
	echo "COMMAND: $COMMAND" >> "$RESULT"
	eval "$COMMAND" >> "$SAMPLE" 2>> "$RESULT"
	if [ "$CLIENT" = true ] ; then
		sleep 5
	fi
done
