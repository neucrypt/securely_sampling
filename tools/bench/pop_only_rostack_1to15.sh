#!/bin/bash

# Not used for anything in the paper, just benches pop-only obliv stack from 1 to 15 levels

mkdir -p ../../benchmark_results/pop_only_rostack/results
mkdir -p ../../benchmark_results/pop_only_rostack/samples

OUTPUT_FILE_NAME=pop_rostk_`date '+%Y-%m-%d~%H-%M-%S'`.txt

SAMPLE_FILE=../../benchmark_results/pop_only_rostack/samples/$OUTPUT_FILE_NAME
RESULT_FILE=../../benchmark_results/pop_only_rostack/results/$OUTPUT_FILE_NAME

touch $SAMPLE_FILE
touch $RESULT_FILE

set -e

CLIENT=false
BENCH_PROG="../../build/tests/bench_pop_rostk"
BENCH_PROG_ARGS="-p 50000"
SIZES=(3 9 21 45 93 189 381 765 1533 3069 6141 12285 24573 49149 98301)
ITERS=(1000 1000 1000 1000 1000 10000 10000 10000 10000 10000 20000 40000 80000 160000 320000)

while getopts ":c:" opt; do
	case $opt in
		c)
			BENCH_PROG_ARGS+=" -c $OPTARG "
			CLIENT=true
			;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			exit 1
			;;
		:)
			echo "Option -$OPTARG requires an argument." >&2
			exit 1
			;;
	esac
done

for ((II=0; II<${#SIZES[*]}; II++));
do
	COMMAND="$BENCH_PROG $BENCH_PROG_ARGS -e ${SIZES[II]} -i ${ITERS[II]}"
	echo "COMMAND: $COMMAND"
	echo "COMMAND: $COMMAND" >> $RESULT_FILE
	eval "$COMMAND" >> $SAMPLE_FILE 2>> $RESULT_FILE
	if [ "$CLIENT" = true ] ; then
		sleep 5
	fi
done
