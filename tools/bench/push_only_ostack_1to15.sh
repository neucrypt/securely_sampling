#!/bin/bash

# Benches push-only obliv stack from 1 to 15 levels, sample data from size 6141 and up can be used to reconstruct our AND gates by operation # graphs

mkdir -p ../../benchmark_results/push_only_ostack/results
mkdir -p ../../benchmark_results/push_only_ostack/samples

OUTPUT_FILE_NAME=push_ostk_`date '+%Y-%m-%d~%H-%M-%S'`.txt

SAMPLE_FILE=../../benchmark_results/push_only_ostack/samples/$OUTPUT_FILE_NAME
RESULT_FILE=../../benchmark_results/push_only_ostack/results/$OUTPUT_FILE_NAME

touch $SAMPLE_FILE
touch $RESULT_FILE

set -e

CLIENT=false
BENCH_PROG="../../build/tests/bench_push_ostk"
BENCH_PROG_ARGS="-p 50000"
SIZES=(3 9 21 45 93 189 381 765 1533 3069 6141 12285 24573 49149 98301)
#ITERS=(3 3 3 3 3 3 3 3 3 3 3 3 1 1 1)

while getopts ":c:" opt; do
	case $opt in
		c)
			BENCH_PROG_ARGS+=" -c $OPTARG "
			CLIENT=true
			;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			exit 1
			;;
		:)
			echo "Option -$OPTARG requires an argument." >&2
			exit 1
			;;
	esac
done

for ((II=0; II<${#SIZES[*]}; II++));
do
	COMMAND="$BENCH_PROG $BENCH_PROG_ARGS -e ${SIZES[II]}"
	echo "COMMAND: $COMMAND"
	echo "COMMAND: $COMMAND" >> $RESULT_FILE
	eval "$COMMAND" >> $SAMPLE_FILE 2>> $RESULT_FILE
	if [ "$CLIENT" = true ] ; then
		sleep 3
	fi
done

