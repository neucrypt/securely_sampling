#used for accurate complexity analysis
POP_GATES = [8.985, 20.961, 30.68, 39.252, 47.163, 55.1694, 62.7675, 70.2666, 77.673, 84.7542, 92.27, 99.778]
PUSH_GATES = [10.333, 19.666, 25.88, 30.989, 35.199, 39.029, 42.686, 46.262, 49.799, 53.317, 56.825, 60.329, 63.831, 67.332, 70.833]
MUX_GATES = [0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4]

# main function, outputs complexity analysis and bench program flags that can be copy and pasted
# NOTE: writing epsilon in the form log(2)*2^-i will let the function know that there is some number of periodic binary expansions 
def findDPparams(epsilon, delta, n):
	eps = epsilon/2
	logn = log(n, 2)
	lam = log(1/delta, 2) + 2
	min_kappa = min([k for k in range(1, 20) if (2*e^-(eps*2^k - log(n))) < 2^-(lam - 1)])
	f = ceil(lam + logn + log(min_kappa, 2))
	k = findk(min_kappa, eps, f)
	howManyPreds = simplify(-log(eps/log(2), 2))
	if howManyPreds in ZZ: 
		predcomplxty = predcomplexity(f, howManyPreds, k)
		popcomplxty = popcomplexity(f, howManyPreds, k)
	else: 
		predcomplxty = predcomplexity(f, k, k)
		popcomplxty = popcomplexity(f, k, k)
	push_stksize = ideal_push_stksize(lam, n, k, predcomplxty, popcomplxty)
 	rem_group_size = mod(n, push_stksize['size'])
	print("Parameters for our algorithm that lead to an (%s, 2^%s)-DP implementation" % (epsilon, simplify(log(delta,2))))
	print("number of pushes (cg) is %f\n" % push_stksize['numpush'])
	print("group size (g) is %f\n" % push_stksize['size'])
	print("number of pushes for rem group (rem_cg) is %f\n" % push_stksize['rem_push'])
	print("rem group size (rem_g) is %f\n" % push_stksize['rem_size'])
	print("kappa is %f\n" % min_kappa)
	print("k is %f\n" % k)
	print("exact delta less than delta truth value: %s\n" % (numerical_approx((2*e^-(eps*2^min_kappa - log(n)) + 2^-lam + 2^-lam), prec=200) < numerical_approx(delta)))
	print("f (len) is %f (%f is avg coin cost for odo)\n" % (f, 2*(f-1)))
	print("popcomplexity is %f\n" % popcomplxty)
	print("predcomplexity is %f\n" % predcomplxty) 
	print(push_stksize['stkorpred'] + " should be used for maximum speed\n")
	print("average coin cost is %f\n" % push_stksize['avgcoincost'])
	ispred = int(push_stksize['stkorpred'] == 'pred')
	print("args for bench program would be: -k %d -l %d -u %d -g %d -v %d -q %d -n %d -r %d\n" % 
		(k, f, push_stksize['numpush'], push_stksize['size'],push_stksize['rem_push'],push_stksize['rem_size'],n, ispred))
	print("args for odo bench would be: -k %d -l %d -n %d\n" % (k, f-1, n))

# finds minimum k for a given kappa, epsilon, and bias length
def findk(kappa, eps, f):
	return min([kappa]+[k for k in range(1, kappa + 1) if (1/(1 + e^(eps*2^k))) < 2^-f])

# computes the probability that pushes will not make stksize coins
def probtoofewcoins(pushes, stksize):
	D = RealDistribution('beta', [pushes - stksize + 1, stksize])
	return D.cum_distribution_function(0.5)

# computes avg pred complexity over all k biases
def predcomplexity(size, howMany, k):
	num5bit = ceil(size/32)
	num6bit = ceil(size/64)
	return (howMany/k)*(5*num6bit + sum(MUX_GATES[0:num6bit]) + 2*ceil(log(size, 2))-1) + ((k-howMany)/k)*(2*ceil(log(size, 2))-1)

# computes avg pop complexity over all k biases
def popcomplexity(size, howMany, k):
	lvls = ceil(log(size/3 + 1, 2))
	return (howMany/k)*(POP_GATES[lvls-1] + lvls-1) + ((k-howMany)/k)*(2*ceil(log(size, 2))-1)

# computes the ideal (most efficient) values for cg (number of pushes to have desired chance of failure) and g (stack size)
# as well as their remainder counterparts 
def ideal_push_stksize(lam, n, k, predcomplexity, popcomplexity):
	min = {}
	min['genbiascost'] = predcomplexity
	min['stkorpred'] = 'pred'
	if (popcomplexity < predcomplexity): 
		min['genbiascost'] = popcomplexity
		min['stkorpred'] = 'stk'
	s = 3
	lvl = 0
	min_val = oo
	while (s <= 98301):
		numpush = search_numpush(2*s, 400*s, s, ceil(n/s), lam, n, k)
		avg = (numpush/s)*(PUSH_GATES[lvl] + min['genbiascost'])
		lvls, rem_s, rem_push = 1, 0, 0
		if (n%s > 0):
			lvls = ceil(log((n%s)/3 + 1, 2))
			rem_s = 3*(2^(lvls)-1)
			rem_push = search_numpush(2*rem_s, 400*rem_s, rem_s, ceil(n/s), lam, n, k)
		cand = avg*s*k*floor(n/s) + k*rem_push*(PUSH_GATES[lvls-1] + min['genbiascost']) + (numpush*k*floor(n/s)+rem_push*k)
		true_avg = cand/(n*k)
		#print(cand) 
		if cand < min_val:
			min_val = cand
			min['numpush'] = numpush
			min['size'] = s
			min['rem_size'] = rem_s
			min['rem_push'] = rem_push 
			min['avgcoincost'] = true_avg
		s = 2*s + 3
		lvl += 1
	return min

# for a given size s, number of groups groups, and security param lam, finds the minimum number of pushes p 
# such that P(p pushes make less than s coins for any group) < 2^-lam 
def search_numpush(low, high, s, groups, lam, n, k):
	if high == low + 1: 
		if probtoofewcoins(low, s) < 2^-(lam + log(groups, 2) + log(k, 2)):
			return low
		else: return high
	else:
		mid = ceil((low + high)/2)
		if probtoofewcoins(mid, s) < 2^-(lam + log(groups, 2) + log(k, 2)):
			return search_numpush(low, mid, s, groups, lam, n, k)
		else: return search_numpush(mid, high, s, groups, lam, n, k)
